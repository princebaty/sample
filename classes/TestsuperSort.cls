//edited in r2
@isTest
public class TestsuperSort {
    
    public static testMethod void sortAscendingTest(){
        
        List<Opportunity> opps = new List<Opportunity>();
        for(integer i = 0; i<1000; i++){
            opps.add(new Opportunity(Name = 'test' + i, Amount = 1000 * Math.random()));
        }
        
        Test.startTest();
        Long start = system.currentTimeMillis();
        superSort.sortList(opps,'Amount','asc');
        //system.debug(system.currentTimeMillis() - start);
        Test.stopTest();
        
        //Assert the list was sorted correctly
        Decimal assertValue = -1;
        for(Opportunity o : opps) {
            //System.debug('Opp value: ' + o.amount);
            System.assert(assertValue <= o.amount);
            assertValue = o.amount;
        }  
    }
    
    public static testMethod void sortDescendingTest(){
        
        List<Opportunity> opps = new List<Opportunity>();
        for(integer i = 0; i<1000; i++){
            opps.add(new Opportunity(Name = 'test' + i, Amount = 1000 * Math.random()));
        }
        
        Test.startTest();
        superSort.sortList(opps,'Amount','desc');
        Test.stopTest();
        
        //Assert the list was sorted correctly
        Decimal assertValue = 1001;
        for(Opportunity o : opps) {
            //System.debug('Opp value: ' + o.amount);
            System.assert(assertValue >= o.amount);
            assertValue = o.amount;
        }  
    }
    
    public static testMethod void sortTest3(){
        
        Account objAcc = new Account(Name='Test Acc');
        insert objAcc;
        
        List<Opportunity> opps = new List<Opportunity>();
        for(integer i = 0; i<2; i++){
            opps.add(new Opportunity(Name = 'test' + i, Amount = 1000 * Math.random(), AccountId=objAcc.Id));
        }
        
        Test.startTest();
        superSort.sortList(opps,'AccountId','asc');
        Test.stopTest();
       
    }
}