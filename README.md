# Salesforce CPQ
-------------------
This repository contains the work performed as part of POC to demonstrate the capability of Salesforce CPQ for various liner products of Maersk.
Link to Jira Userstories - [CPQ](https://jira.maerskdev.net/projects/CPQ)

## Packages Involved
-------------------
1. **[Salesforce CPQ](https://appexchange.salesforce.com/appxListingDetail?listingId=a0N300000016cxaEAA)**   
CPQ simplifies configuration and ensures pricing and quoting accuracy. It includes *Product Bundling, Guided Selling, Pricing and Product Rules, Advanced Quote Line Editor, Quote Templates and Generation*
Following notable objects are part of package.
    * Quote ( `SBQQ__Quote__c` ) 
    * Quote Line ( `SBQQ__QuoteLine__c` )
    * Product Rule ( `SBQQ__ProductRule__c` )
    * Price Rule ( `SBQQ__PriceRule__c` )
    * Product Option ( `SBQQ__ProductOption__c` )  
  
  
2. **[Salesforce Billing](https://appexchange.salesforce.com/appxListingDetail?listingId=a0N300000016cxaEAA)**   
It is an add-on to salesforce cpq and provides to *create Orders, Automate your billing, Generate Invoices automatically, Manage Subscriptions and Invoice in Multiple Currencies, Collect Payments using your preferred gateway.*
    * Billing Schedule ( `blng__BillingSchedule__c` ) 
    * Invoice ( `blng__Invoice__c` )
    * Payment ( `blng__Payment__c` )  
  
  
3. **Advanced Approvals**   
Support complex approval processes with dynamic approvers and complex aggregate criteria. Add on to salesforce cpq.
    * Approval ( `sbaa__Approval__c` ) 
    * Approver ( `sbaa__Approver__c` )  
  
  
Note : Refer the package.xml in the ../src/ folder for list of components.
  
  
## Recommended Steps for Data Import
-----------
* #### Quote Process (Guided Selling)  
  
    1. Import the *Quote Process* (`SBQQ__QuoteProcess__c`) record.  
  
    2. Import *Process Input* (`SBQQ__ProcessInput__c`) with following fields :  
  
        | Field         | API Name                 | Comment                                            |
        |---------------|--------------------------|----------------------------------------------------|
        | Quote Process | `SBQQ__QuoteProcess__c`  | Master-Detail relationship field                   |
        | Input Field   | `SBQQ__InputField__c`    | Field API name that holds the values for selection |
        | Product field | `SBQQ__ProductField__c`  | Field in product that will be used for filtering   |
        | Operator      | `SBQQ__Operator__c`      |                                                    |
  
    3. If any of the process input is dependent on another input then insert *Process Input Condition* (`SBQQ__ProcessInputCondition__c`) record :  
  
        | Field | API Name | Comment |
        |-------|---------|--------------------|
        | Process Input | `SBQQ__ProcessInput__c` | Master-Detail relationship field, Regulated process input |
        | Master Process Input | `SBQQ__MasterProcessInput__c` | Driving Process Input |
        | Value | `SBQQ__ProductField__c` | Matching value of driving process input |
        | Operator | `SBQQ__Operator__c` |  |
  
##  
* #### Product Configuration (Bundling)
    Import the object records in following order :
    
    | Order | Object          | API Name                  | Relationship field(s)        | Comment |
    |-------|-----------------|---------------------------|------------------------------|---------|
    | 1     | Product2        | `Product2`                |                              |         |
    | 2     | Product Feature | `SBQQ__ProductFeature__c` | `SBQQ__ConfiguredSKU__c` (1) |         |
    | 3     | Product Option  | `SBQQ__ProductOption__c`  | `SBQQ__Feature__c` (2)       |         |
    | 4     | Configuration Attribute | `SBQQ__ConfigurationAttribute__c` | `SBQQ__Feature__c` (2) | optional. used for filtering |
  
  
    Note : The (1/2/3) in relationship field  column refers to the order row object. For example, in above table, (1) means `Product2` object reference  
  
##  
* #### Product Rule
    Import the object records in following order :
    
    | Order | Object             | API Name                          | Relationship field(s)      | Comment                          |
    |-------|--------------------|-----------------------------------|----------------------------|----------------------------------|
    | 1     | Product Rule       | `SBQQ__ProductRule__c`            |                            |                                  |
    | 2     | Error Condition    | `SBQQ__ErrorCondition__c`         | `SBQQ__Rule__c` (1)        |                                  |
    | 3     | Lookup Query       | `SBQQ__LookupQuery__c`            | `SBQQ__ProductRule__c` (1) | optional                         |
    | 4     | Product Action     | `SBQQ__ConfigurationAttribute__c` | `SBQQ__Rule__c` (1)        | optional if lookup query is used |
    | 5     | Configuration Rule | `SBQQ__ConfigurationRule__c`      | `SBQQ__ProductRule__c` (1) |                                  |
  
##  
* #### Price Rule
    Import the object records in following order :
    
    | Order | Object | API Name | Relationship field(s) | Comment |
    |------|--------|---------|------------------------|---------|
    | 1 | Price Rule | `SBQQ__PriceRule__c` | | |
    | 2 | Price Condition | `SBQQ__PriceCondition__c` | `SBQQ__Rule__c` (1) | |
    | 3 | Lookup Query | `SBQQ__LookupQuery__c` | `SBQQ__PriceRule2__c` (1) | optional |
    | 4 | Price Action | `SBQQ__ConfigurationAttribute__c` | `SBQQ__Rule__c` (1) | optional if lookup query is used |
  
##  
* #### Quote Template
    Import the object records in following order :
    
    | Order | Object | API Name | Relationship field(s) | 
    |------|--------|---------|------------------------|
    | 1 | Quote Template | `SBQQ__QuoteTemplate__c` | 
    | 2 | Line Column | `SBQQ__LineColumn__c` | `SBQQ__Template__c` (1) 
    | 3 | Template Content | `SBQQ__TemplateContent__c` | |  
    | 4 | Template Section | `SBQQ__TemplateSection__c` | `SBQQ__Template__c` (1), `SBQQ_Content__c` (3) |  
    | 5 | Quote Term | `SBQQ__QuoteTerm__c` |  |
  
##  
### Notable Components
-------------------

* #### [CPQ-245](https://jira.maerskdev.net/browse/CPQ-245) - Connection with MEPC
[../src/pages/MECP_POC.page](https://git.maerskdev.net/projects/SF/repos/cpq-poc-work/browse/src/pages/MECP_POC.page) --
This page accepts input from user and then makes an api call to MEPC. Parse the JSON response and then displays it in structured form.
##  
* #### [CPQ-237](https://jira.maerskdev.net/browse/CPQ-237) - Expose product API to external systems
[../src/classes/CPQ_API.cls](https://git.maerskdev.net/projects/SF/repos/cpq-poc-work/browse/src/classes/CPQ_API.cls) --
This webservice class have @HttpGet method which will look for four parameters productFamily, shipmentType, country, port and return the list of products along with prices and additional details accordingly.
  
### Project status
----------
It was a POC and no longer worked upon.